#!/bin/bash

files=(vimrc gvimrc emacs bashrc zshrc tmux.conf Xdefaults zshrc_cygwin vimrc_cygwin)

for i in "${files[@]}"
do
  rm  ~/.$i
  ln -s `pwd`/$i ~/.$i
done

LXDE_RC="~/.config/openbox/lxde-rc.xml"
rm $LXDE_RC
ln -s lxde-rc.xml $LXDE_RC

