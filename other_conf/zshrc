# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
#ZSH_THEME="af-magic"
#ZSH_THEME="clean"
ZSH_THEME="example"


# Example aliases
alias zshconfig="v ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=$PATH:/home/ducalpha/bin:/home/ducalpha/adr/sdk/tools:/home/ducalpha/adr/sdk/platform-tools:/home/ducalpha/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/home/ducalpha/adr/ndk/toolchains/arm-linux-androideabi-4.6/prebuilt/linux-x86/bin

export TERM=xterm-256color

alias ls='ls --color -h --group-directories-first'
alias l='ls'
export USE_CCACHE=1
export OUT_DIR_COMMON_BASE=/home/ducalpha/kernel/build
# export CROSS_COMPILE=/home/ducalpha/adr/arm-eabi-4.6/bin/arm-eabi-
#alias adrsrc="cd ~/kernel/versions/android_master"
#alias adrbuild="cd /home/ducalpha/kernel/build/android/android_master/"
alias adrsrc="cd /home/ducalpha/kernel/versions/android-4.1.2_r1"
alias adrbuild="cd /home/ducalpha/kernel/build/android/android-4.1.2_r1/"

ANDROID_NDK=/home/ducalpha/adr/ndk
export PATH=$PATH:${ANDROID_NDK}/toolchains/arm-linux-androideabi-4.6/prebuilt/linux-x86/bin
alias agcc="arm-linux-androideabi-gcc --sysroot=${ANDROID_NDK}/platforms/android-14/arch-arm -fPIC -mandroid -DANDROID -DOS_ANDROID"
alias asl='adb shell'
alias logcat='adb logcat'

stty ixany
stty ixoff -ixon
alias setarm='export PLATFORM=arm'
alias setx86='export PLATFORM=x86'
alias c='cd'
alias v='vi'
alias g='grep'
alias cv='cd ~/v50'
alias cs='cd ~/sling'
alias css='cd ~/sling/Source'
alias csb='cd ~/sling/WebKitLibraries/sling/src/external/libbase-chromium/'
alias csc='cd ~/sling/Source/Core'
alias ct='cv && cd third_party/catapult/telemetry/telemetry'
alias cf='cv && cd tools/perf'
alias cm='cf && cd measurements'
alias cb='cf && cd benchmarks'
alias cg='cf && cd page_sets'
alias cbr='cb && cd browser_profiler'
alias cn='cv && cd net'
alias ca='cf && cd analysis'  # /mnt/sda5/chromium/31.0.1650.59_mod/out/Release'
alias ck='ca && cd apks'
alias cw='cv && cd third_party/WebKit/Source'
alias cr='~/projects/gchrome_biglittle/replay/us_all/'
alias vd='vim ~/diff_chromium'
alias ce='cd ~/eBrowser_patches'
alias cdp='cd ~/cleanup_techniques/content/shell/browser/profiler'

#alias enw='\emacs -nw -name -bg black -fg white -bd ForestGreen'
alias e='emacs -nw' # no windows
#alias emacsx='\emacs -name Editor_Emacs_$USER@$HOST -geometry 80x50 -bg black -fg white -bd ForestGreen'
#alias emacs='enw' 
alias list='tmux list-sessions' 
alias attach='tmux attach' 

export PATH=$PATH:$HOME/.vim/local/bin:$HOME/tools/tv
export PATH=$PATH:$HOME/tools/depot_tools
export CLEWNDIR=$HOME/.vim/bundle/pyclewn/macros
export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox

unsetopt cdablevars


alias ssl='sdb shell'
export PATH=$PATH:/usr/local/texlive/2015/bin/x86_64-linux
export PATH=$PATH:~/tools/depot_tools
export INFOPATH=$INFOPATH:/usr/local/texlive/2015/texmf-dist/doc/info
export MANPATH=$MANPATH:/usr/local/texlive/2015/texmf-dist/doc/man
#export ANDROID_NDK_PATH=~/adr/ndk

eval `dircolors ~/tools/dircolors-solarized/dircolors.256dark`

# added by Anaconda2 4.0.0 installer
#export PATH="/home/ducalpha/anaconda2/bin:$PATH"
#export PATH=$PATH:~/build/ducalpha/projects/naver/chromium/src/third_party/android_tools/sdk/build-tools/23.0.1/

if [ "$MY_SETTING" = "" ]; then
  # setxkbmap -option caps:escape
  # xrdb ~/.Xdefaults
  export MY_SETTING="set";
fi

alias lc='adb logcat'
alias lcr='lc -s chromium'
alias lcc='lc -c'

# eval `ssh-agent`
eval `keychain --eval id_rsa`

setopt noincappendhistory
setopt nosharehistory
unset GNOME_KEYRING_CONTROL
alias vizrc='vi ~/.zshrc'
alias synczrc='. ~/.zshrc'
alias em='emacsclient -t'

. ~/bin/mx

# use vim mode for zsh shell
#set -o vi
if [ "$TMUX" = "" ]; then
  echo 'Welcome';
  byobu new;
fi

### ZCA's installer added snippet ###
fpath=( "$fpath[@]" "$HOME/.config/zca/zsh-cmd-architect" )
autoload h-list zca zca-usetty-wrapper zca-widget
zle -N zca-widget
bindkey '^T' zca-widget
### END ###

# add this configuration to ~/.zshrc
export HISTFILE=~/.zsh_history  # ensure history file visibility
export HH_CONFIG=hicolor        # get more colors

. /usr/share/autojump/autojump.sh
export EDITOR=vim
setopt menu_complete


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
